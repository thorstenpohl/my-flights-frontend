'use strict';

/**
 * @ngdoc overview
 * @name openflightsApp
 * @description
 * # openflightsApp
 *
 * Main module of the application.
 */
angular
  .module('openflightsApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'ui.bootstrap.datetimepicker',
    'ui.bootstrap.modal',
    'ngCookies'
  ])
  .factory('accessTokenInjector', [
        '$q','$rootScope', function($q, $rootScope) {
    return {
        request: function (config) {
            var deferred = $q.defer();
            if ($rootScope.keycloak && $rootScope.keycloak.token) {
                $rootScope.keycloak.updateToken(5).success(function() {
                    config.headers = config.headers || {};
                    config.headers.Authorization = 'Bearer ' + $rootScope.keycloak.token;

                    deferred.resolve(config);
                }).error(function() {
                        deferred.reject('Failed to refresh token');
                    });
            }
            return deferred.promise;
        }
    };
  }
]).config([
        '$httpProvider', function($httpProvider) {
            $httpProvider.interceptors.push('accessTokenInjector');
        }
])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/addFlight', {
        templateUrl: 'views/addflight.html',
        controller: 'AddflightCtrl',
        controllerAs: 'addFlight'
      })
      .when('/editFlight/:flightId', {
        templateUrl: 'views/addflight.html',
        controller: 'AddflightCtrl',
        controllerAs: 'addFlight'
      })
      .when('/flightlist', {
        templateUrl: 'views/flightlist.html',
        controller: 'FlightlistCtrl',
        controllerAs: 'flightlist'
      })
      .when('/flight/add', {
        templateUrl: 'views/flight/add.html',
        controller: 'FlightAddCtrl',
        controllerAs: 'flight/add'
      })
      .otherwise({
        redirectTo: '/'
      });
  }).run(function ($http, $interval, $rootScope) {
    var keycloakConfig ={
            "realm": "myflights",
            "realm-public-key": "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAx4slXuOTbKgqZXkwDOkMWK3Zy3jGl8sxXJI/q6qvQ9aiCu5Yp/dl6SIh4TVVXsB1emDuGoNZxakIv+fTJr7FUrb0IvLn/mBwJy6w4uOsUKGUfYRgLjD1RUvpHyd33g1qwYROCuixroft5BSoY0MSO4BNgCAgP3GhMzBHyUxaNGrRSqKRcWmUy2Il/86yxufIVsPGmfXJWFZ1Al6u3qS9jlZCoDRwlpLnq+T8FDJt1MOz1fE4ELX9Hal2D5XNiqp13fclxRVBf1Mq5B7uMiAANlkmnFzuE4LahiK3/2FUG4D6GKqEWQdz2Pgsg2o4hhqoumq0nMC3BSkpUEZBKrw40wIDAQAB",
            "url": "https://keycloak-pohl.rhcloud.com/auth",
    "ssl-required": "external",
            "clientId": "myflights-frontend",
            "credentials": {
              "secret": "57a9d746-bd3e-46cd-9339-cf3b4e4f4417"
            }
          };
    if (!$rootScope.keycloak){
      var keycloakAuth = new Keycloak(keycloakConfig);
      console.log("log in required");
      keycloakAuth.init({ onLoad: 'login-required' })
        .success(function () {
          console.log("logged in");
          $rootScope.keycloak = keycloakAuth;
          console.log($rootScope.keycloak);
        })
        .error(function () {
          console.log("ERROR");
          window.location.reload();
      });
    }
});
