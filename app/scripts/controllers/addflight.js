'use strict';

/**
 * @ngdoc function
 * @name openflightsApp.controller:AddflightCtrl
 * @description # AddflightCtrl Controller of the openflightsApp
 */
angular.module('openflightsApp').controller(
		'AddflightCtrl',

				function($scope, $rootScope,flightService, autocomplete, BACKEND_URL,$routeParams, $resource, $http,
						$uibModal) {
							$scope.flight={};
					if ($routeParams.flightId){
						flightService.load($routeParams.flightId).then(
								function(response) {
									$scope.flight = response.data
								}, function() {
									console.log("error")
								});
					}




					var openDialog = function(saveResult) {
						var modalInstance = $uibModal.open({
							animation : true,
							templateUrl : 'addFlightSuccess.html',
							controller : 'AddflightModalCtrl',
							resolve : {
								saveResult : function() {
									return saveResult;
								}
							}
						});
						modalInstance.result.then(function() {
							$scope.flight = {};
						});

					}

					$scope.save = function() {

						var result = flightService.save($scope.flight);
						openDialog(result);

					};
					$scope.updateDate = function() {
						$scope.flight.date = null;
						$scope.flight.date = null;
					}

					$scope.openCalendar = function(event, what) {
						if ('departure' == what) {
							$scope.cal_open_departure = true;
						} else if ('arrival' == what) {
							$scope.cal_open_arrival = true;
						}
					}

					$scope.autocomplete = function() {
						console.log("autocomplete()")
						$http.post(
								BACKEND_URL + "flight-autocomplete/flightNo",
								$scope.flight).then(function(response) {
							$scope.flight = response.data;

						}, function() {
							console.log("error")
						});
					};
					$scope.loadMasterdata = function() {
						console.log("masterdata()")
						if ($scope.from == "") {
							$scope.flight.from = null;
						} else {
							autocomplete.airport(
									$scope.from).then(
									function(response) {
										$scope.flight.from = response.data
									}, function() {
										console.log("error")
									});
						}
						if ($scope.to == "") {
							$scope.flight.to = null;
						} else {
							autocomplete.airport(
									$scope.to).success(
									function(data) {
										$scope.flight.to = data;
									});
						}
						// $http.get(
						// BACKEND_URL + "/masterdata/aircraft/"
						// + $scope.flight.acType).then(
						// function(response) {
						// $scope.aircraft = response.data
						// }, function() {
						// console.log("error")
						// });
					}
				}

);

// Controller for the Modal Dialog.
angular.module('openflightsApp').controller(
		'AddflightModalCtrl',
		[ '$scope', '$uibModalInstance', 'saveResult',
				function($scope, $uibModalInstance, saveResult) {

					// This does not seam to work.
					if (saveResult === "true") {
						$scope.message = "Success";
					} else {
						$scope.message = "Failed.";
					}

					$scope.ok = function() {
						$uibModalInstance.close();
					};

					$scope.cancel = function() {
						$uibModalInstance.dismiss('cancel');
					};
				} ]);
