'use strict';

/**
 * @ngdoc function
 * @name openflightsApp.controller:FlightAddCtrl
 * @description
 * # FlightAddCtrl
 * Controller of the openflightsApp
 */
angular.module('openflightsApp')
  .controller('FlightAddCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
