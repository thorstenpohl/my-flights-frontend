'use strict';

/**
 * @ngdoc function
 * @name openflightsApp.controller:FlightlistCtrl
 * @description # FlightlistCtrl Controller of the openflightsApp
 */
angular.module('openflightsApp').controller(
		'FlightlistCtrl',

				function($scope, $window, flightService) {
					$scope.window={};
					var listFlights = function(){
					flightService.list().success(function(data){
								$scope.flights = data;
								var parameterArray =$scope.flights.map(function (flight) {
									if (flight.from && flight.to){
										return "&P="+flight.from.code+"-"+flight.to.code;
									} else {
										return "";
									}
								});
								$scope.mapParameters=parameterArray.join("");

								$scope.window.innerWidth = $window.innerWidth;
							});
					};
					listFlights();


				} );
