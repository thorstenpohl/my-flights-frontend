'use strict';

/**
 * @ngdoc service
 * @name openflightsApp.autocomplete
 * @description
 * # autocomplete
 * Service in the openflightsApp.
 */
angular.module('openflightsApp')
  .service('autocomplete', function ($http, BACKEND_URL) {
    this.airport = function(airportCode){
      return $http.get(
          BACKEND_URL + "masterdata/airport/"+airportCode);
    }
    // AngularJS will instantiate a singleton by calling "new" on this function
  });
