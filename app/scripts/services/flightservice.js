'use strict';

/**
 * @ngdoc service
 * @name openflightsApp.flightService
 * @description
 * # flightService
 * Service in the openflightsApp.
 */
angular.module('openflightsApp')
  .service('flightService', function ($http, $resource, BACKEND_URL) {

    var flights = $resource(BACKEND_URL + "flight", {}, {
      save : {
        method : 'POST'
      }
    });

    this.list=function(){
      return $http.get(BACKEND_URL+"flight");
    };

    this.load = function(flightId){
      return $http.get(
          BACKEND_URL + "flight/"
              + flightId);
    }

    this.save = function(flight){
      return flights.save(flight);
    }

  });
