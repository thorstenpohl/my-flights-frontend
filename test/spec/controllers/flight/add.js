'use strict';

describe('Controller: FlightAddCtrl', function () {

  // load the controller's module
  beforeEach(module('openflightsApp'));

  var FlightAddCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FlightAddCtrl = $controller('FlightAddCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(FlightAddCtrl.awesomeThings.length).toBe(3);
  });
});
